import xlrd
import xlsxwriter
import ipaddress
import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formatdate
from email import encoders
import jtextfsm
import os


# FTP credentials
FTP_UN_OFF = os.environ['FTP_UN_OFF']
FTP_PW_OFF = os.environ['FTP_PW_OFF']
FTP_HOST_OFF = os.environ['FTP_HOST_OFF']
FTP_UN_ON = os.environ['FTP_UN_ON']
FTP_PW_ON = os.environ['FTP_PW_ON']
FTP_HOST_ON = os.environ['FTP_HOST_ON']

# source configs for upgrade
SRC_F_ASR920 = os.environ['SRC_F_ASR920']     
SRC_F_ISR4200 = os.environ['SRC_F_ISR4200']   
SRC_F_ISR4300 = os.environ['SRC_F_ISR4300']   
SRC_F_ISR4400 = os.environ['SRC_F_ISR4400'] 

# hashes of source configs for upgrade
SRC_F_ASR920_HASH = os.environ['SRC_F_ASR920_HASH']  
SRC_F_ISR4200_HASH = os.environ['SRC_F_ISR4200_HASH']  
SRC_F_ISR4300_HASH = os.environ['SRC_F_ISR4300_HASH']   
SRC_F_ISR4400_HASH = os.environ['SRC_F_ISR4400_HASH'] 

# credentials for sender email to function
SMTP_SERVER = os.environ['SMTP_SERVER']
SMTP_PORT = int(os.environ['SMTP_PORT'])
SENDER_EMAIL = os.environ['SENDER_EMAIL']
SENDER_PWD = os.environ['SENDER_PWD']

FTP_CREDENTIALS = {
    'off': f'{FTP_UN_OFF}:{FTP_PW_OFF}@{FTP_HOST_OFF}',
    'on': f'{FTP_UN_ON}:{FTP_PW_ON}@{FTP_HOST_ON}'
}

SRC_F = {
    'ASR-920': SRC_F_ASR920,
    'ISR-4200': SRC_F_ISR4200,
    'ISR-4300': SRC_F_ISR4300,
    'ISR-4400': SRC_F_ISR4400
}

SRC_F_HASH = {
    'ASR-920': SRC_F_ASR920_HASH,
    'ISR-4200': SRC_F_ISR4200_HASH,
    'ISR-4300': SRC_F_ISR4300_HASH,
    'ISR-4400': SRC_F_ISR4400_HASH
}

ON_NET_IP_RANGE = '198.18.0.0/15'

FREE_MEMORY_LOWER_BOUND = 565000000  # bytes


# categorize IPs
def check_IPs(IPs, on_net=True):
    on_net_ip_network = ipaddress.ip_network(ON_NET_IP_RANGE)

    valid_IPs = []
    invalid_IPs = []
    for IP in IPs:
        if on_net:
            valid_IPs.append(IP) if ipaddress.ip_address(
                IP) in on_net_ip_network else invalid_IPs.append(IP)
        else:
            valid_IPs.append(IP) if ipaddress.ip_address(
                IP) not in on_net_ip_network else invalid_IPs.append(IP)

    return valid_IPs, invalid_IPs


# read out IPs in a .xlsx file
def read_IPs_from_xlsx(path_to_f):
    with xlrd.open_workbook(path_to_f) as f:
        f_sheet = f.sheet_by_index(0)

        IPs = set()
        for i in range(f_sheet.nrows):
            for j in range(f_sheet.ncols):
                cell_val = f_sheet.cell_value(i, j).strip()
                try:
                    ipaddress.ip_address(cell_val)
                except:
                    continue
                IPs.add(cell_val)

        return IPs


# extract device model form the output of 'show version' command
def extract_model_from_out(out):
    with open('textfsm/show_version.textfsm') as template:
        table = jtextfsm.TextFSM(template)
    out_parsed = table.ParseText(out)
    if not out_parsed:
        return None
    else:
        model = out_parsed[0][0]
        if model.startswith('ASR'):
            return 'ASR-920'
        elif model.startswith('ISR4'):
            return 'ISR-4' + model[4] + '00'
        else:
            return model
    # return None if not out_parsed else out_parsed[0][0]


# extract the number of free bytes from the output of 'dir' command
# e.g. 7194652672 bytes total (5825789952 bytes free)
def extract_free_bytes_from_out(out):
    out_split1 = out.split('(')
    out_split2 = out_split1[1].split(' ', 1)
    return out_split2[0]


# extract the result info for the 'copy' command
def extract_cp_info_from_out(out):
    out_split = out.rsplit('\n', 3)
    return ' '.join([i for i in out_split[-3:] if i])


# write data into a .xlsx file
def write_data_to_xlsx(data, f_path, ls_item=False):
    with xlsxwriter.Workbook(f_path) as b:
        b_sheet = b.add_worksheet()

        len_data = len(data)
        if ls_item:
            for i in range(len_data):
                for j in range(len(data[i])):
                    b_sheet.write(i, j, data[i][j])
        else:
            for i in range(len_data):
                b_sheet.write(i, 0, data[i])
    return f_path


# order results in out_order
def order_results(ress, out_order: list):
    ress_dict = {}
    for res in ress:
        ress_dict[res[0]] = res

    for i in range(len(out_order)):
        out_order[i] = ress_dict[out_order[i]]

    return ress


# send out an email
def send_email_with_att(msg_dtls: dict, html=False, att=True):
    context = ssl.create_default_context()
    with smtplib.SMTP(SMTP_SERVER, SMTP_PORT) as server:
        server.ehlo()  # can be omitted
        server.starttls(context=context)
        server.ehlo()  # can be omitted
        server.login(SENDER_EMAIL, SENDER_PWD)

        fake_from = msg_dtls.get('fake_from')
        to = msg_dtls['to']
        subject = msg_dtls['subject']
        content = msg_dtls['content']
        path_att = msg_dtls['att']['path']
        name_att = msg_dtls['att']['name']

        msg = MIMEMultipart()
        msg['From'] = SENDER_EMAIL if not fake_from else fake_from
        msg['To'] = ', '.join(to)
        msg['Subject'] = subject
        msg.attach(MIMEText(content + '\n\n\n')
                   ) if not html else msg.attach(MIMEText(content + '<br><br><br>', 'html'))

        if att:
            att = MIMEBase('application', 'octet-stream')
            att.set_payload(open(path_att, 'rb').read())
            encoders.encode_base64(att)
            att.add_header('Content-Disposition',
                           f'attachment; filename="{name_att}"')
            msg.attach(att)

        server.sendmail(SENDER_EMAIL, to, str(msg))


# custom exception
class Custom_Error(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __repr__(self):
        return self.msg
